const axios = require(PLUGIN_DIR + 'axios.js')

exports.topHeadlines = async (req, res) => {
    try {
        // let resp = await axios.get('v2/everything');
        let resp = await axios.get('v2/top-headlines', {
            params: {
                country: "us",
                apiKey: "6f7cab6382cb4f7e993f43cb6aeedda7",
            }
        });

        let data = resp.data;
        console.log(data);
        res.render('index', {data: data, raw: JSON.stringify(data)});

    } catch (err) {
        console.log(err.message)
    }
    // const apiKey = "6f7cab6382cb4f7e993f43cb6aeedda7";
    // fetch("https://newsapi.org/v2/top-headlines?country=us&apiKey="+apiKey)
    //     .then(response => response.json())
    //     .then(json => console.log(json))
}

