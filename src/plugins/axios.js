const axios = require('axios').create({
    baseURL: 'https://newsapi.org',
    // baseURL: 'https://jsonplaceholder.typicode.com',
    timeout: 3000,
    headers: {
        // 'X-Api-Key': '6f7cab6382cb4f7e993f43cb6aeedda7',
        // 'Authorization': 'Bearer 6f7cab6382cb4f7e993f43cb6aeedda7',
    }
});

module.exports = axios;