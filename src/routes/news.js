const express = require('express');
const router = express.Router();

const {topHeadlines} = require(ROOT_DIR + "/src/controllers/newsController")

router.get('/', topHeadlines)

module.exports = router;