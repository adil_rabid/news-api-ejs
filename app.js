const express = require('express');
const {text} = require("express");
const app = express();
const port = 8080;

global.ROOT_DIR = __dirname;
global.PLUGIN_DIR = ROOT_DIR + '/src/plugins/';

app.use(express.json());

// headers
// app.use((req, res, next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//     res.setHeader("Access-Control-Allow-Headers", "Content-type, Authorization");
//     next();
// })

app.use(express.static('./public'));
app.use('/css', express.static(__dirname + 'public/css'));
app.use('/js', express.static(__dirname + 'public/js'));
app.use('/img', express.static(__dirname + 'public/img'));


//Set views
app.set('views', __dirname + '/src/views');
app.set('view engine', 'ejs');

// routes
const newsRoutes = require(__dirname + '/src/routes/news.js');
app.use(newsRoutes);


app.listen(port, () => {
    console.log("Server running at http://localhost:" + port);
})

